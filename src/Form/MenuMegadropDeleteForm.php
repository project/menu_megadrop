<?php

namespace Drupal\menu_megadrop\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Menu megadrop entities.
 *
 * @ingroup menu_megadrop
 */
class MenuMegadropDeleteForm extends ContentEntityDeleteForm {


}
